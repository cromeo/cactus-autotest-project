Checklist for cactus.kh.ua
1. Check login menu with valid credentials
2. Check login menu with invalid credentials
3. Check search field with correct values by ENTER KEY
4. Check search field with incorrect values
5. Check search field with correct values by button
6. Check comparing menu
7. Check Adding to cart
8. Check deleting from cart
9. Check feedback form to Head of cactus.kh.ua
10. Check feedback form with empty fields
11. Check navigation menu and catalog by dropdown list
12. Check return top button from footer on page
