package mainPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FeedbackLetterPage extends BasePage {
    public FeedbackLetterPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "//*[@class='fa fa-pencil']")
    WebElement sendFeedbackButton;
    @FindBy (xpath = "//*[@class='form-director']")
    public WebElement formFeedback;
    @FindBy (xpath = "//*[@name='contact_name']")
    WebElement nameInputForm;
    @FindBy (xpath = "//*[@name='contact_message']")
    WebElement messageInputForm;
    @FindBy (xpath = "//*[@name='contact_email']")
    WebElement emailInputForm;
    @FindBy (xpath = "//*[@name='contact_phone']")
    WebElement phoneInputForm;
    @FindBy (xpath = "//*[@class='btn btn--default director-submit']")
    WebElement submitFormButton;
    @FindBy (xpath = "//*[contains (text(), 'Ваше письмо отправлено.')]")
    public WebElement sendingSuccess;
    @FindBy (xpath = "//*[@class='small alert_error' and contains (text(), 'Пожалуйста, заполните необходимые поля')]")
    public WebElement alertMessageRequiredFields;







    public FeedbackLetterPage openFeedbackForm(){
        sendFeedbackButton.click();
        return this;
    }

    public FeedbackLetterPage submitFormSend (){
        submitFormButton.click();
        return this;
    }
    public FeedbackLetterPage fillName (){
        nameInputForm.sendKeys("MyName");
        return this;
    }
    public FeedbackLetterPage fillEmail (){
        emailInputForm.sendKeys("myemail@gmail.com");
        return this;
    }
    public FeedbackLetterPage fillPhone (){
        phoneInputForm.sendKeys("0991234567");
        return this;
    }
    public FeedbackLetterPage fillMessage (){
        messageInputForm.sendKeys("I'm sorry, but i need to test your feedback form");
        return this;
    }

}
