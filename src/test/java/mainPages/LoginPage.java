package mainPages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class  LoginPage extends BasePage {

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);

    }

    private String loginEmail = "cromeo780@gmail.com";
    private String loginPass = "password01";
    private String wrongEmail = "qwerty@gmail.com";
    private String wrongPass = "qwertypass";
    @FindBy(xpath = "//*[@id=\"email\"]")
    public WebElement emailField;
    @FindBy(xpath = "//*[@id=\"passwd\"]")
    public WebElement passwordField;
    @FindBy(xpath = "//*[@id=\"SubmitLogin\"]")
    public WebElement SubmitLogin;
    @FindBy(xpath = "//*[@class='page-title' and contains(text(), 'Личный кабинет')]")
    public WebElement loginSuccess;
    @FindBy(xpath = "//*[contains(text(), 'Ошибка авторизации.')]")
    public WebElement loginFailed;

    @Step
    public LoginPage fillLoginValues (){
        emailField.sendKeys(loginEmail);
        passwordField.sendKeys(loginPass);
        return this;
    }
    @Step
    public LoginPage fillWrongEmail (){
        emailField.sendKeys(wrongEmail);
        passwordField.sendKeys(loginPass);
        return this;
    }
    @Step
    public LoginPage fillWrongPASS (){
        emailField.sendKeys(loginEmail);
        passwordField.sendKeys(wrongPass);
        return this;
    }
    @Step
    public LoginPage submitLog (){
        SubmitLogin.click();
        return this;
    }


}
