package mainPages;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header extends BasePage {

    public Header(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//*[@class=\"login\"]")
    WebElement loginLink;

    @Step
    public Header clickLogin() {
        loginLink.click();
        return this;
    }
    @Step
    public LoginPage fillEmailAndPass() {
        LoginPage loginPage = new LoginPage(driver).fillLoginValues();
        return new LoginPage(driver);
    }
    @Step
    public LoginPage fillWrongEmailAndCorrectPass() {
        LoginPage loginPage = new LoginPage(driver).fillWrongEmail();
        return new LoginPage(driver);
    }
    @Step
    public LoginPage fillCorrectEmailAndWrongPass() {
        LoginPage loginPage = new LoginPage(driver).fillWrongPASS();
        return new LoginPage(driver);
    }
    public JavascriptExecutor jse = (JavascriptExecutor)driver;

    {
        jse.executeScript("window.scrollBy(0,250)", "");
    }
}
