package mainPages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;

import java.util.List;

public class SearchPage extends BasePage {
    public SearchPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);

    }

    String searchIphone = "Apple iPhone XS Max 64 Гб (Серебристый) MT512";
    String searchSamsung = "Samsung Galaxy S10 2019 6/128Gb Black (SM-G973FZKDSEK) UA";
    String searchHuawei = "Huawei P30 Pro 6/128Gb Black (51093TFV) UA";

    @DataProvider
    public Object[][] getInputValues() {
        return new Object[][]{
                {"iphone"},
                {"samsung"},
                {"xiaomi"},
        };
    }

    @FindBy(xpath = "//*[@class=\"subcategory-image\"]")
    public WebElement searchResultList;
    @FindBy(xpath = "//*[@id=\"search_query_block\"]")
    WebElement searchField;
    @FindBy(xpath = "//*[contains(text(), '0 совпадений было найдено.')]")
    public WebElement noResult;
    @FindBy(xpath = "//*[contains(text(), 'Пожалуйста, укажите зарпос для поиска')]")
    public WebElement errorResult;
    @FindBy(xpath = "//*[@id='search_button']")
    WebElement searchButton;
    @FindBy(xpath = "//*[@class='add_to_compare btn-add-to-compare ']")
    public WebElement compareButton;
    @FindBy(xpath = "//*[contains(text(), 'Перейти к сравнению')]")
    WebElement compareMenu;
    @FindBy(xpath = "//*[@class=\"fancybox-item fancybox-close\"]")
    WebElement closeCompareButton;
    @FindBy(xpath = "//*[contains(text(), 'Сравнение товаров')]")
    public WebElement compareTitleMenu;
    @FindBy(xpath = "//*[contains(text(), 'Вы не добавили для сравнения ни одного товара')]")
    public WebElement messageWithoutCompare;
    @FindBy(xpath = "//*[@src='https://cactus.kh.ua/img/compare.svg']")
    public WebElement compareImageButton;
    @FindBy (xpath = "//*[@title='Купить']")
    WebElement addButtonToCart;
    @FindBy (xpath = "//*[@title='Закрыть окно']")
    WebElement closeButtonWindow;
    @FindBy (xpath = "//*[@alt='Корзина']")
    WebElement cartButtonIcon;
    @FindBy(xpath = "//*[@class='cart_block_product_name' and @title='Apple iPhone XS Max 64 Гб (Серебристый) MT512']")
    public WebElement itemInCartDisplayed;
    @FindBy(xpath = "//*[@class='remove_link']")
    WebElement removeFromCartButton;
    @FindBy (xpath = "//*[@class='cart-items-count count ajax_cart_quantity' and contains(text(), '0')]")
    public WebElement alertEmptyCart;

    @Step
    public SearchPage searchByTextByEnter(String text) {
        searchField.sendKeys(text + Keys.ENTER);
        return this;
    }

    @Step
    public SearchPage searchBySearchButton(String text) {
        searchField.sendKeys(text);
        searchButton.click();

        return this;
    }

    @Step
    public SearchPage searchWithoutText() {
        searchField.sendKeys(Keys.ENTER);
        return this;
    }

    @Step
    public SearchPage comparingTest() {
        searchField.sendKeys(searchIphone + Keys.ENTER);
        compareButton.click();
        closeCompareButton.click();
        searchField.clear();
        searchField.sendKeys(searchHuawei + Keys.ENTER);
        compareButton.click();
        closeCompareButton.click();
        searchField.clear();
        searchField.sendKeys(searchSamsung + Keys.ENTER);
        compareButton.click();
        compareMenu.click();
        return this;
    }

    @Step
    public SearchPage deleteFromCompare(){
        List<WebElement> closeButtons = driver.findElements(By.xpath("//*[@title='Удалить из списка']"));
        for (WebElement i: closeButtons
             ) {i.click();

        }
        compareImageButton.click();
    return this;}

    @Step
    public SearchPage addItemToCart (){
        searchByTextByEnter(searchIphone);
        addButtonToCart.click();
        closeButtonWindow.click();
        driver.navigate().refresh();

        cartButtonIcon.click();


        return this;}

    @Step
    public SearchPage removeItemFromCart(){
        removeFromCartButton.click();
        cartButtonIcon.click();
        return this;
    }
}
