package mainPages;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class MainPage extends BasePage {

    public MainPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    JavascriptExecutor jse = (JavascriptExecutor) driver;


    @FindBy(xpath = "//*[@class='nav']")
    public WebElement topNavigationMenu;

    @FindBy(xpath = "//*[@class='logo']")
    WebElement mainLogo;

    @FindBy(xpath = "//*[@id='scrollUp']")
    WebElement buttonToUpPage;

    @FindBy(xpath = "//*[contains(text(),'Самовывоз из магазина Кактус в Харькове')]")
    public WebElement deliveryPage;

    @FindBy(xpath = "    //*[@class='vc_inner']\n")
    public WebElement mainPageElement;

    @FindBy(xpath = "//*[@href='/content/4-delivery']")
    WebElement deliveryButton;

    @Step
    public MainPage scrollDonw() {
        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        return this;
    }


    @Step
    public MainPage scrollTopClick() {
        buttonToUpPage.click();
        return this;
    }

    @Step
    public MainPage checkDeliveryMenu() {
        deliveryButton.click();
        return this;
    }

    @Step
    public MainPage returnMain() {
        mainLogo.click();
        return this;
    }


}