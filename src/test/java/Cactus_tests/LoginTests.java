package Cactus_tests;

import mainPages.Header;
import mainPages.LoginPage;
import org.junit.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {
//    private WebDriver driver;



    @Test(description = "check succesfull login")
    public void checkLogin() {
//        driver.get("https://cactus.kh.ua/");
        LoginPage loginPage = new Header(driver).clickLogin().fillEmailAndPass().submitLog();
        Assert.assertTrue(loginPage.loginSuccess.isDisplayed());
    }

    @Test(description = "incorrect values using for login email")
    public void checkFailLoginEmail() {
        LoginPage loginPage = new Header(driver).clickLogin().fillWrongEmailAndCorrectPass().submitLog();
        Assert.assertTrue(loginPage.loginFailed.isDisplayed());
    }

    @Test(description = "incorrect values using for login password")
    public void checkFailLoginPassword() {
        LoginPage loginPage = new Header(driver).clickLogin().fillCorrectEmailAndWrongPass().submitLog();
        Assert.assertTrue(loginPage.loginFailed.isDisplayed());
    }





}
