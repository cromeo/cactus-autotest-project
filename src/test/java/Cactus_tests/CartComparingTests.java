package Cactus_tests;

import mainPages.SearchPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CartComparingTests extends BaseTest {

    @Test (description = "success comparing items")
public void checkCompareItems ()  {

    SearchPage searchPage = new SearchPage(driver).comparingTest();
    Assert.assertTrue(searchPage.compareTitleMenu.isDisplayed());
}

@Test (description = "success deleting items from compare menu")
    public void checkClearComparing (){
SearchPage searchPage = new SearchPage(driver).comparingTest().deleteFromCompare();
Assert.assertTrue(searchPage.messageWithoutCompare.isDisplayed(), "element with error message is displayed");
}

@Test (description = "success adding to cart")
    public void checkAddItemToCart () {
    SearchPage searchPage = new SearchPage(driver).addItemToCart();
    Assert.assertTrue(searchPage.itemInCartDisplayed.isDisplayed(), "Item was added to cart and displayed");
}

@Test (description = "success removing from cart")
    public void removeFromCart (){
    SearchPage searchPage = new SearchPage(driver).addItemToCart().removeItemFromCart();
    Assert.assertTrue(searchPage.alertEmptyCart.isDisplayed(), "alert message is displayed");
}
}
