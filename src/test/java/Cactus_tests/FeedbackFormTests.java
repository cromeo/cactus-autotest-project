package Cactus_tests;

import mainPages.FeedbackLetterPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FeedbackFormTests extends BaseTest {
    @Test(description = "successful form appears")
    public void checkFormAppear() {
        FeedbackLetterPage feedbackLetterPage = new FeedbackLetterPage(driver).openFeedbackForm();
        Assert.assertTrue(feedbackLetterPage.formFeedback.isDisplayed(), "feedback form is displayed");
    }

    @Test (description = "successful feedback sending")
    public void sendFormWithCorrectValues(){
        FeedbackLetterPage feedbackLetterPage = new FeedbackLetterPage(driver).openFeedbackForm().fillEmail()
                .fillMessage().fillName().fillPhone().submitFormSend();
        Assert.assertTrue(feedbackLetterPage.sendingSuccess
                .isDisplayed(), "alert message with confirm sending");
    }

    @Test (description = "check required fields")
    public void sendFormWithoutName(){
        FeedbackLetterPage feedbackLetterPage = new FeedbackLetterPage(driver).openFeedbackForm()
                .fillPhone().fillEmail().fillMessage().submitFormSend();
        Assert.assertTrue(feedbackLetterPage.alertMessageRequiredFields.isDisplayed(), "Alert message is displayed");
    }


}