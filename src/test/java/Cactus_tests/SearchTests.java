package Cactus_tests;

import mainPages.SearchPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SearchTests extends BaseTest {

    @DataProvider
    public Object[][] getInputValues() {
        return new Object[][]{
                {"iphone"},
                {"samsung"},
                {"xiaomi"},
        };
    }
    @Test(description = "succesfull search by field", dataProvider = "getInputValues")
    public void checkSuccessSearch(String inputValue) {
        SearchPage searchPage = new SearchPage(driver).searchByTextByEnter(inputValue);
        Assert.assertTrue(searchPage.searchResultList.isDisplayed(), "Result by search is displayed");
    }
    @Test (invocationCount = 5, description = "negative search by incorrect value")
    public void checkSearchByWrongValue (){
        SearchPage searchPage = new SearchPage(driver).searchByTextByEnter("qweqweqweqwe");
        Assert.assertTrue(searchPage.noResult.isDisplayed(), "result by search is NOT founded");
    }
    @Test (description = "search without value")
    public void checkSearchWithoutValue (){
        SearchPage searchPage = new SearchPage(driver).searchWithoutText();
        Assert.assertTrue(searchPage.errorResult.isDisplayed(), "message with text for fill field");
    }

    @Test (description = "correct search by button")
    public void checkSearchByButton(){
        SearchPage searchPage = new SearchPage(driver).searchBySearchButton("iphone");
        Assert.assertTrue(searchPage.searchResultList.isDisplayed(), "Result by search is displayed");
    }
}
