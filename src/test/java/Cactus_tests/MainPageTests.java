package Cactus_tests;

import mainPages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MainPageTests extends BaseTest {

    @Test(description = "successful working of scrollup button")
    public void checkScrollup() {
        MainPage mainPage = new MainPage(driver).scrollDonw().scrollTopClick();
        Assert.assertTrue(mainPage.topNavigationMenu.isDisplayed(), "element with navigation is displayed");
    }

    @Test(description = "successful redirect to delivery page")
    public void checkDeliveryPage() {
        MainPage mainPage = new MainPage(driver).checkDeliveryMenu();
        Assert.assertTrue(mainPage.deliveryPage.isDisplayed(), "delivery page appears");
        mainPage.returnMain();
        Assert.assertTrue(mainPage.mainPageElement.isDisplayed(), "redirect to main page");
    }

}
